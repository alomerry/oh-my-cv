pipeline {
  // 环境变量
  environment {
    url = 'https://gitlab.com/alomerry/oh-my-cv.git'
    barkDevice = credentials('bark-notification-device-alomerry')
    cdnDomain = credentials('cdn-domain')
    BUILD_NUMBER = "${env.BUILD_NUMBER}"
  }
  // pipeline 的触发方式
  triggers {
    GenericTrigger(
      genericVariables: [
        [ key: 'name', value: '$.repository.name', expressionType: 'JSONPath' ],
        [ key: 'branch', value: '$.ref', expressionType: 'JSONPath' ],
      ],
      printContributedVariables: false,
      printPostContent: false,
      tokenCredentialId: 'webhook-trigger-token',
      regexpFilterText: '$name@$branch',
      regexpFilterExpression: 'oh-my-cv@refs/heads/develop',
      causeString: ' Triggered on $branch' ,
    )
  }
  agent {
      docker {
          image 'registry.cn-hangzhou.aliyuncs.com/alomerry/blog-build:latest'
          args '-v /etc/timezone:/etc/timezone:ro -v /etc/localtime:/etc/localtime:ro'
      }
  }
  stages {
    stage('pull code') {
        steps {
            retry(3) {
                // 拉取代码
                git(url: env.url, branch: 'develop')
            }
        }
    }
    stage('install and build') {
      steps {
        retry(3) {
          sh 'pnpm install && pnpm build:pkg && pnpm build'
        }
      }
    }
      stage('compress') {
        steps {
          // 压缩构建后的文件用于发布到服务器的 nginx 中
          retry(3) {
            sh '''
            cd /var/jenkins_home/workspace/resume/site/dist/
            tar -zcf resume.tar.gz *
            '''
          }
        }
      }
      stage('ssh') {
        steps {
          script {
            def remote = [:]
            remote.name = 'root'
            remote.logLevel = 'FINEST'
            remote.host = 'resume.alomerry.com'
            remote.allowAnyHosts = true
            withCredentials([usernamePassword(credentialsId: 'tencent-vps-admin', passwordVariable: 'password', usernameVariable: 'username')]) {
              remote.user = "${username}"
              remote.password = "${password}"
            }
            sshCommand remote: remote, command: '''#!/bin/bash
              cd /root/apps/nginx/site/resume.alomerry.com/
              shopt -s extglob
              rm -rf !(.htaccess|.user.ini|.well-known|favicon.ico|resume.tar.gz)
              '''
            sshPut remote: remote, from: '/var/jenkins_home/workspace/resume/site/dist/resume.tar.gz', into: '/root/apps/nginx/site/resume.alomerry.com/'
            sshCommand remote: remote, command: "cd /root/apps/nginx/site/resume.alomerry.com && tar -xf resume.tar.gz"
            sshRemove remote: remote, path: '/root/apps/nginx/site/resume.alomerry.com/resume.tar.gz'
          }
        }
      }
  }
  post {
    success {
      sh 'curl --globoff "https://bark.alomerry.com/$barkDevice/Resume%20build%20status%3A%20%5B%20Success%20%5D?icon=https%3A%2F%2F${cdnDomain}%2Fmedia%2Fimages%2Fjenkins.png&isArchive=0&group=jenkins&sound=electronic&level=passive"'
    }
    failure {
      sh 'curl --globoff "https://bark.alomerry.com/$barkDevice/Resume%20build%20status%3A%20%5B%20sFailed%20%5D?icon=https%3A%2F%2F${cdnDomain}%2Fmedia%2Fimages%2Fjenkins.png&url=https%3A%2F%2Fci.alomerry.com%2Fjob%2Fresume%2F${BUILD_NUMBER}%2Fconsole&isArchive=0&group=jenkins&sound=electronic"'
    }
  }
}
