const request = require('request');
const fs = require('node:fs');

run()

function run() {
  let writeStream = downloadResume()
  writeStream.on("close", function (err) {
    if (err) {
      console.log("download resume failed.")
    } else {
      replaceAlomerryResume(getLatestResumeContent())
    }
  });
}

function downloadResume() {
  let tmp = fs.createWriteStream("./site/src/utils/constants/resume.md");
  let resume = "https://gitee.com/alomerry/blog/raw/master/src/about/resume/2023.md"
  return request(resume).pipe(tmp)
}

function getLatestResumeContent() {
  let resumePath = "./site/src/utils/constants/resume.md"
  let latestResume = fs.readFileSync(resumePath, 'utf8');
  return "export const ALOMERRY_MD_CONTENT = `" + latestResume + "`"
}

function replaceAlomerryResume(latestResumeContent) {
  let tsPath = "./site/src/utils/constants/alomerry.ts"
  let tsContent = fs.readFileSync(tsPath, 'utf8');
  let reg = /export const ALOMERRY_MD_CONTENT = `[<>\/\(\)\+\.\w\s\n:="-@\u4e00-\u9fa5，、~（）：]*`/g
  let result = tsContent.replace(reg, latestResumeContent)
  fs.writeFileSync(tsPath, result, 'utf8')
}