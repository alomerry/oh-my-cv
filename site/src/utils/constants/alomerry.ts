import type { ResumeStyles } from "~/types";

export const ALOMERRY_STYLES = {
  marginV: 55,
  marginH: 45,
  lineHeight: 1.3,
  paragraphSpace: 5,
  themeColor: "#025AA7",
  fontCJK: {
    name: "华康宋体",
    fontFamily: "HKST"
  },
  fontEN: {
    name: "Times Newer Roman"
  },
  fontSize: 15,
  paper: "A4"
} as ResumeStyles;

const ALOMERRY_SELECTOR = "#vue-smart-pages-preview";

export const ALOMERRY_MD_CONTENT = `---
name: Alomerry Wu
header:
  - text: <span class="iconify" data-icon="tabler:phone"></span> (+86) 13064929335
  - text: <span class="iconify" data-icon="tabler:mail"></span> alomerry.wu@gmail.com
    link: alomerry.wu@gmail.com
  - text: <span class="iconify" data-icon="charm:person"></span> Blog
    link: https://blog.alomerry.com
  - text: <span class="iconify" data-icon="tabler:brand-github"></span> GitHub
    link: https://github.com/alomerry
  - text: <span class="iconify" data-icon="ic:outline-location-on"></span> Shanghai
    newLine: true
---

## TODO

计算机基础

- 算法
- 数据结构 https://javaguide.cn/cs-basics/data-structure/linear-data-structure.html
- 计算机网络 https://javaguide.cn/cs-basics/network/other-network-questions.html
- 操作系统 https://javaguide.cn/cs-basics/operating-system/operating-system-basic-questions-01.html

数据库 https://javaguide.cn/database/nosql.html

- MongoDB https://javaguide.cn/database/mongodb/mongodb-questions-01.html，https://www.pdai.tech/md/interview/x-interview.html#_8-4-mongodb
- MySQL
- Redis

常用框架

分布式

- CAP 理论 和 BASE 理论、Paxos 算法和 Raft 算法 https://javaguide.cn/distributed-system/theorem&algorithm&protocol/cap&base-theorem.html
- RPC、服务注册与发现
- 分布式事务
- 分布式 ID

高并发

- 消息队列
- 读写分离&分库分表 https://javaguide.cn/high-performance/read-and-write-separation-and-library-subtable.html
- 负载均衡

高可用 https://javaguide.cn/high-availability/high-availability-system-design.html

- 限流
- 降级
- 熔断

搜索引擎

- ES
- Solar

认证

- SSO
- OAuth2


## 教育经历

**某某学校**

本科 软件工程
  ~ 09/2015 - 07/2029

## 工作经历

**某 SCRM 公司**

后端开发工程师
  ~ 09/2015 - 07/2029

## 项目经历

**UA 积分体系定制**
  ~ 核心开发者
  ~ 2019.06 - 至今

- 基于 Go-Chi 封装的自研框架，存储使用 TiDB，ES7 和 Redis，Kafka 和 Pulsar 进行异步处理和延时消息处理，由企业平台（销售组织架构管理）、用户画像、企微二维码 & 标签、电话短信跟进等能力构成，围绕意向 - 线索的概念进行销售对用户的跟进和转化
- 参与销售中台部分功能搭建，重构已有的模板方法+策略模式的核心流程，抽象出通用实现，利用 option 模式实现为不同类型产生的意向和线索处理组装独有的分配流程
- 优化 Binlog 同步 ES 处理，对已有的类 canal 同步工具进行并行消费改造，避免 Kafka 消费积压，支持同步工具针对 ES 多个集群的同步写入，应对 ES 服务宕机场景
- 针对正价课用户退费场景，参与退费审批需求，自己基于有向无环图实现和搭建轻量工作流框架，提供模板钩子函数支持工作流调用时的前后置校验，支持后续多种业务场景调用实现流转
- 参与公海库需求，为销售中台做前置工作，区分销售私有线索和已战败的公有线索池，利用 goevaluate 实现简易线索战败规则表达式校验
- 参与 Beanstalk 异步任务改造，利用 Pulsar 延时消息特性进行重构，支持单 topic 进行多种任务的延迟和立刻消费

**DHC 按交易的等级成长模式**
  ~ 核心开发者
  ~ 2019.06 - 至今

- 基于 Go-Chi 封装的自研框架，存储使用 TiDB，ES7 和 Redis，Kafka 和 Pulsar 进行异步处理和延时消息处理，由企业平台（销售组织架构管理）、用户画像、企微二维码 & 标签、电话短信跟进等能力构成，围绕意向 - 线索的概念进行销售对用户的跟进和转化
- 参与销售中台部分功能搭建，重构已有的模板方法+策略模式的核心流程，抽象出通用实现，利用 option 模式实现为不同类型产生的意向和线索处理组装独有的分配流程
- 优化 Binlog 同步 ES 处理，对已有的类 canal 同步工具进行并行消费改造，避免 Kafka 消费积压，支持同步工具针对 ES 多个集群的同步写入，应对 ES 服务宕机场景
- 针对正价课用户退费场景，参与退费审批需求，自己基于有向无环图实现和搭建轻量工作流框架，提供模板钩子函数支持工作流调用时的前后置校验，支持后续多种业务场景调用实现流转
- 参与公海库需求，为销售中台做前置工作，区分销售私有线索和已战败的公有线索池，利用 goevaluate 实现简易线索战败规则表达式校验
- 参与 Beanstalk 异步任务改造，利用 Pulsar 延时消息特性进行重构，支持单 topic 进行多种任务的延迟和立刻消费

**零售活动处理，MongoDB 驱动修改，工具撰写**
  ~ 核心开发者
  ~ 2019.06 - 至今

- knowledge-lib（客服知识库平台）
  - 基于 SpringBoot、ES、Vue 搭建的客服知识库平台，提供文章、词条、问答等多种类型数据的搜索和聚合供客服查阅，帮助客服提效
  - 参与知识库智能搜索项目的算法侧工程共建，算法侧对热搜词和输入框输入后匹配的引导语进行计算后，将数据存储在 Redis ZSet，工程侧进行去重、过滤，并根据权重使用优先队列计算 Top N 结果
- tpcc2（统一监控平台2期）
  - 基于 SpringBoot 和 Vue 开发的客服数据处理监控平台，大数据侧将用户进线咨询事件单、售后单等单据数据推数存储到 MySQL，统一监控平台侧定时任务拉取进行数据清洗和处理同步到 ES，给客服人员提供以多维度筛选能力，根据单据本身的预警类型和级别在 CRM 系统介入处理
  - 参与售后单和纠纷单接入需求，为区别于已有事件单，基于注解实现策略模式，拆分不同单据类型的特殊和通用处理逻辑，根据传入参数的不同字段组合进行路由
- exam（客服考试系统）
  - 基于 Spring、定时任务组件 tb 调度和 MongoDB 的考试系统，实现对客服的日常考核、业务素质检验，tb 调度定时任务在管理员创建试卷 + 试题后，按开考时间进行调度，创建试卷副本并接入 CRM 系统通知，提醒客服开考，交卷后自动判题（仅选择题）并统计各部门客服作答情况
  - 参与众客服改造，为内部员工客服和外部众客服同时提供服务，基于拦截器实现外网 pin 和内网 erp 登录鉴权和系统菜单权限控制（前端jQuery，比较老的系统，具体逻辑记不清了）

## 奖项

**PAT 甲级**：排名 1/23432
  ~ 2019.06

**PAT 甲级**：排名 234/23432
  ~ 2018.06

**PAT 甲级**：排名 234/23432
  ~ 2018.06

## 技术栈

- 熟悉 Go 语言，了解 GMP 模型，map、channel 等数据结构，对 Gin 等框架中的 Mux 设计有一定了解
- 熟悉 Java 语言，了解 JVM、多线程等特性，了解 Spring(Boot) 框架，有开发经验并了解基本原理
- 熟悉 ElasticSearch，了解基本原理和使用
- 熟悉 MySQL 使用和基本原理，了解 borm/Gorm 等 ORM 框架
- 有数据结构和简单算法基础，并能在工作中应用
- 了解 TiDB、Redis、Kafka、Pulsar 的基本原理并有使用经验
- 有 Git 使用经验和 Linux 环境下编程经验，了解 TCP、HTTP 基本原理
- 基本的英语读写能力，能读懂英文文档，善用搜索引擎


定时任务执行时长超过 Redis 锁预期时长存在的问题
带 _id 的多字段排序`;

export const ALOMERRY_CSS_CONTENT = `/* Backbone CSS for Resume Template */

#vue-smart-pages-preview {
  padding-bottom: 200px!important;
}

.vue-smart-page-break {
  display: none;
}

.new-line {
  height: 5px;
}

${ALOMERRY_SELECTOR} hr{
  margin: 0 0 6px 0;
}

/* Basic */

${ALOMERRY_SELECTOR} {
  background-color: white;
  color: black;
  text-align: justify;
  -moz-hyphens: auto;
  -ms-hyphens: auto;
  -webkit-hyphens: auto;
  hyphens: auto;
}

${ALOMERRY_SELECTOR} p,
${ALOMERRY_SELECTOR} li,
${ALOMERRY_SELECTOR} dl {
  margin: 0;
}

/* Headings */

${ALOMERRY_SELECTOR} h1,
${ALOMERRY_SELECTOR} h2,
${ALOMERRY_SELECTOR} h3 {
  font-weight: bold;
}

${ALOMERRY_SELECTOR} h1 {
  font-size: 2.13em;
}

${ALOMERRY_SELECTOR} h2,
${ALOMERRY_SELECTOR} h3 {
  margin-bottom: 5px;
  font-size: 1.2em;
}

${ALOMERRY_SELECTOR} h2 {
  border-bottom-style: solid;
  border-bottom-width: 1px;
}

/* Lists */

${ALOMERRY_SELECTOR} ul,
${ALOMERRY_SELECTOR} ol {
  padding-left: 1.5em;
  margin: 0.2em 0;
}

${ALOMERRY_SELECTOR} ul {
  list-style-type: circle;
}

${ALOMERRY_SELECTOR} ol {
  list-style-type: decimal;
}

/* Definition Lists */

${ALOMERRY_SELECTOR} dl {
  display: flex;
}

${ALOMERRY_SELECTOR} dl dt,
${ALOMERRY_SELECTOR} dl dd:not(:last-child) {
  flex: 1;
}

/* Tex */

${ALOMERRY_SELECTOR} :not(span.katex-display) > span.katex {
  font-size: 1em !important;
}

/* SVG & Images */

${ALOMERRY_SELECTOR} svg.iconify {
  vertical-align: -0.2em;
}

${ALOMERRY_SELECTOR} img {
  max-width: 100%;
}

/* Header */

${ALOMERRY_SELECTOR} .resume-header {
  text-align: center;
}

${ALOMERRY_SELECTOR} .resume-header h1 {
  text-align: center;
  line-height: 1;
  margin-bottom: 8px;
}

${ALOMERRY_SELECTOR} .resume-header-item:not(.no-separator)::after {
  content: " | ";
}

/* Citations */

${ALOMERRY_SELECTOR} ul.crossref-list {
  padding-left: 1.2em;
}

${ALOMERRY_SELECTOR} li.crossref-item p {
  margin-left: 0.5em;
}

${ALOMERRY_SELECTOR} li.crossref-item::marker {
  content: attr(data-caption);
}

${ALOMERRY_SELECTOR} sup.crossref-ref {
  font-size: 100%;
  top: 0;
}

/* Dark & print mode */

.dark ${ALOMERRY_SELECTOR} {
  background-color: #334155;
  color: #e5e7eb;
}

@media print {
  ${ALOMERRY_SELECTOR} {
    background-color: white !important;
    color: black !important;
  }

  .dark ${ALOMERRY_SELECTOR} a {
    color: black !important;
  }
}
`;
